## Solution

Ta cần tìm số nguyên dương $x$ không lớn hơn $n$ và thêm $x^{2}, x^{3}$ vào tập $S$ nếu nó không vượt quá $n$. 

Kết quả là $|S|$.

$S$ là tập chứa các phần tử không trùng nhau. Tập $S$ các bạn cỏ thể sử dụng set. Tham khảo set ở đây: [C++](https://www.cplusplus.com/reference/set/set/), [Python](https://www.w3schools.com/python/python_sets.asp?fbclid=IwAR0gtXVydZPQNSUH3iTZZEa9WQvr2Rb6y-f3bsswFxERcI8t8tByxJgVer0)