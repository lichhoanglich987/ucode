#include<bits/stdc++.h>
using namespace std;
int main()
{
    int t; cin >> t;
    while(t--)
    {
        int n;
        bool ok = 0;
        cin >> n;
        set<int> s;
        for(int i = 2; i <= sqrt(n); ++i)
        {
            int x = i*i;
            int y = i*i*i;
            s.insert(x);
            if(y > n)
                ok = true;
            if(!ok)
                s.insert(y);
        }
        cout << s.size()+1 << endl;
    }
    return 0;
}