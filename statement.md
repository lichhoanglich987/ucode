# Bình Phương và Lập Phương

Nhập vào số nguyên $n$, hãy đếm từ $1$ đến $n$ có bao nhiêu số là số chính phương hoặc số lập phương (hoặc vừa là số chính phương, vừa là số lập phương).

## Input

- Dòng đầu tiên chứa số nguyên $t$ - số lượng bộ test.

- $t$ dòng tiếp theo, mỗi dòng chứa 1 số nguyên $n$.

## Constraints

- $1 ≤ t ≤ 20$.

- $1 ≤ n ≤ 10^{9}$.

## Output

- $t$ dòng, mỗi dòng là số lượng số thỏa mãn yêu cầu ứng với bộ test đó.

## Sample input 1

```
3
10
1
25
```

## Sample output 1
```
4
1
6
```

## Sample input 2

```
3
1000000000
999999999
500000000
```

## Sample output 2
```
32591
32590
23125
```

## Explanation 

- Với $n = 1$ thì các số thỏa mãn là : $1$.
- Với $n = 10$ thì các số thỏa mãn là : $1, 4, 8, 9$.
- Với $n = 25$ thì các số thỏa mãn là : $1, 4, 8, 9, 16, 25$.
